package code;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.concurrent.Semaphore;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import javafx.scene.input.KeyCode;
import roombacomm.*;

public class Roomba_j_d_j_v1 extends Thread implements Runnable
{
	public static final int X_pos = 250;
	public static Roomba_j_d_j_v1 th = new Roomba_j_d_j_v1();
	public static RoombaCommSerial roombacomm = new RoombaCommSerial();
	public static JFrame ventana_ppal = new JFrame("MEN�");
	public static Semaphore s1 = new Semaphore(1);
	public static JTextArea etiqueta = new JTextArea();
	public static JScrollPane scroll = new JScrollPane(etiqueta);
	//public static Move_Thread move = new Move_Thread(roombacomm);
    static String usage = 
        "Uso: \n"+
        "  ejroomba puertoserie\n" +
        "\n";
    static boolean debug = false;
    static boolean hwhandshake = false;
    public Roomba_j_d_j_v1() 
    {
	}
    public static void main(String[] args) throws InterruptedException {
        if( args.length < 1 ) {
            System.out.println( usage );
            System.exit(0);
        }
        String []sensores = new String[19];
        String []sensores_cliff = new String[2];
        boolean giroder = false;
        boolean giroder1 = false;
        
        JButton circulo = new JButton("cir_culo");
        JButton get_rect = new JButton("Rect_culo");
        JButton truan = new JButton("Truan_culo");
        JButton clean = new JButton("clean_culo");
        JButton rand_song = new JButton("rand_culo");
        
        String portname = args[0];  // e.g. "/dev/cu.KeySerial1"
        //roombacomm.setProtocol("SCI"); //Si es un modelo antiguo     
        roombacomm.setProtocol("OI"); //Si es un modelo nuevo 5XX negro
        roombacomm.debug = debug;
        roombacomm.waitForDSR = hwhandshake;
        
        ventana_ppal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana_ppal.setSize(400, 300);
        ventana_ppal.setDefaultLookAndFeelDecorated(true);
        
        etiqueta.setSize(100,150);
        circulo.setSize(100, 40);
        get_rect.setSize(100, 40);
        truan.setSize(100, 40);
        clean.setSize(100, 40);
        rand_song.setSize(100, 40);
        scroll.setSize(100, 150);
        
        circulo.setLocation(X_pos,10);
        scroll.setLocation(0, 0);
        get_rect.setLocation(X_pos,60);
        truan.setLocation(X_pos,110);
        clean.setLocation(X_pos,160);
        rand_song.setLocation(X_pos,210);
        //etiqueta.setForeground(new Color(10, 10, 255));
/*ACTION LISTENERS*/
        circulo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) 
			{
				circulo();
			}
		});;
		get_rect.addActionListener(new ActionListener() 
		{
					public void actionPerformed(ActionEvent arg0) 
					{
						rectangulo();
					}
				});;
		truan.addActionListener(new ActionListener() 
		{
					public void actionPerformed(ActionEvent arg0) 
					{
						triangulo();
					}
				});;
        /*JFRAME ADDS*/
		//scroll.add(etiqueta);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
				
		ventana_ppal.add(circulo);
		ventana_ppal.add(rand_song);
		ventana_ppal.add(clean);
        ventana_ppal.add(get_rect);
        ventana_ppal.add(truan);
        ventana_ppal.add(etiqueta);
        ventana_ppal.add(scroll);
        ventana_ppal.add(new JLabel(new ImageIcon("C:\\Users\\Javier\\Pictures\\r2d2.jpg")));
        ventana_ppal.setVisible(true);
        /*ROOMBA PROGRAM*/
        etiqueta.setEditable(false);
        etiqueta.append("Roomba startup\n");
        //scroll.add(etiqueta);
       // 
        /*if( ! roombacomm.connect( portname ) ) {
            System.out.println("Couldn't connect to "+portname);
           // System.exit(1);
        }*/
        // Inicializar el roomba en modo seguro
        //roombacomm.startup();
       // roombacomm.control();
        //roombacomm.full();
        // Poner el roomba en modo control total
		//roombacomm.updateSensors();
		/*sensores = roombacomm.sensorsAsString().split(" ");
		sensores_cliff = sensores[3].split(":");
		System.out.println(sensores_cliff[1]);
		if(sensores_cliff[1].equals("___r") || sensores_cliff[1].equals("l___"))
		 System.out.println("Que me caigo");*/
		while(true)
		{
			if(keyIsPressed())
			{
				/*roombacomm.stop();
		        System.out.println("Disconnecting");
		        roombacomm.disconnect();
		        System.out.println("Done");*/
		        break;
			}
		}
    }

    /** check for keypress, return true if so */
    public static boolean keyIsPressed() {
        boolean press = false;
        try { 
            if( System.in.available() != 0 ) {
                System.out.println("key pressed");
                press = true;
            }
        } catch( IOException ioe ) { }
        return press;
    }
    public static boolean circulo()
    {
    	//etiqueta.insert("Circulo",etiqueta.getText().length()+1);
    	etiqueta.append("Circulo\n");
    	/*try {
			s1.acquire(1);
			for(int i=0; i<360; ++i)
			{
				roombacomm.goForward(100);
	        	roombacomm.spinRight(1);
			}
			s1.release();
		} catch (InterruptedException e) {
			System.out.println("Error en el circulo.");
		}//Circulo*/
		
    	return true;
    }
    public static boolean rectangulo()
    {
    	//etiqueta.insert("Rectangulo",etiqueta.getText().length()+1);
    	etiqueta.append("Rectangulo\n");
    	/*try {
			s1.acquire(1);
			roombacomm.spinRight(90);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(90);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(90);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(90);
	    	roombacomm.goForward(400);
			s1.release();
		} catch (InterruptedException e) {
			System.out.println("Error en el rectangulo");
		}/*Cuadrado*/
    	return true;
    }
    public static boolean triangulo()
    {
    	etiqueta.append("Triangulo\n");
    	/*try {
			s1.acquire(1);//Triangulo?
			roombacomm.spinRight(60);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(60);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(60);
	    	roombacomm.goForward(400);
	    	roombacomm.spinRight(360);
	    	s1.release();
		} catch (InterruptedException e) {
			System.out.println("Error en el triangulo");
		}*/
    	return true;
    }
}

